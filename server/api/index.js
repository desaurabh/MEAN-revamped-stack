var limit = require("./limit");
var ExpressQuery = require("./ExpressQuery");

function API(req) {
    this.limit = 'undefined';
    this.requestFor = 'undefined';
    var eQuery = new ExpressQuery(req);
    if (eQuery.isUrlEncoded()) eQuery.decodeAndBuild();
    req.query = eQuery.decodedQuery;
    if (req.query.hasOwnProperty('q')) {
        switch (req.query.q) {
            case "limit":
                this.requestFor = "limit";
                this.limit = new limit(req);
                break;
            case "count":
                this.requestFor = "count";
                break;
        }
    }
};
module.exports = API;
