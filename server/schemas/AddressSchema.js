var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var addressSchema = new Schema({
    state:String,
    street: String,
    locality: String,
    city: String,
    country: String,
    pincode: Number
});


module.exports = addressSchema;
